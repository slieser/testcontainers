using dotenv.net;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace testcontainerexample.tests;

[TestFixture]
public class AppSettingsTests
{
    [Test]
    public void Read_appsettings_values() {
        var configurationBuilder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json");
        var configuration = configurationBuilder.Build();
        var host = configuration.GetSection("MongoDb")["Host"];
        Assert.That(host, Is.EqualTo("localhost"));
    }

    public class MongoDbSettings
    {
        public string Host { get; set; } = "";
        public int Port { get; set; }
        public string User { get; set; } = "";
        public string Password { get; set; } = "";
    }

    public MongoDbSettings GetMongoDbSettings(IConfiguration configuration) {
        var mongoDbSettings = new MongoDbSettings();
        configuration.GetSection("MongoDB").Bind(mongoDbSettings);
        return mongoDbSettings;
    }

    [Test]
    public void Read_config_from_appsettings() {
        var configurationBuilder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json");
        var configuration = configurationBuilder.Build();

        var mongoDbSettings = GetMongoDbSettings(configuration);

        Assert.That(mongoDbSettings.Host, Is.EqualTo("localhost"));
        Assert.That(mongoDbSettings.Port, Is.EqualTo(27017));
        Assert.That(mongoDbSettings.User, Is.EqualTo("mongo"));
        Assert.That(mongoDbSettings.Password, Is.EqualTo("secret"));
    }

    [Test]
    public void Read_config_from_environment() {
        DotEnv.Fluent().WithEnvFiles("tests.env").Load();
        
        var configurationBuilder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables();
        var configuration = configurationBuilder.Build();

        var mongoDbSettings = GetMongoDbSettings(configuration);

        Assert.That(mongoDbSettings.Host, Is.EqualTo("127.0.0.1"));     // from tests.env
        Assert.That(mongoDbSettings.Port, Is.EqualTo(27018));           // from tests.env
        Assert.That(mongoDbSettings.User, Is.EqualTo("mongo2"));        // from tests.env
        Assert.That(mongoDbSettings.Password, Is.EqualTo("secret"));    // from appsettings.json
    }

    [Test]
    public void Bind_config_from_environment_variables_only_no_appsettings() {
        DotEnv.Fluent().WithEnvFiles("tests.env").Load();
        File.Move("appsettings.json", "appsettings.json.bak");

        try {
            var configurationBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();
            var configuration = configurationBuilder.Build();
    
            var mongoDbSettings = GetMongoDbSettings(configuration);
    
            Assert.That(mongoDbSettings.Host, Is.EqualTo("127.0.0.1"));     // from tests.env
            Assert.That(mongoDbSettings.Port, Is.EqualTo(27018));           // from tests.env
            Assert.That(mongoDbSettings.User, Is.EqualTo("mongo2"));        // from tests.env
            Assert.That(mongoDbSettings.Password, Is.EqualTo(""));          // not present in tests.env
        }
        finally {
            File.Move("appsettings.json.bak", "appsettings.json");
        }
    }
}