using NUnit.Framework;

namespace testcontainerexample.tests;

[TestFixture]
public class EnvironmentExampleTests
{
    [Test]
    public void Get_unknown_env_variable() {
        var result1 = Environment.GetEnvironmentVariable("UNKNOWN_ENV_VARIABLE");
        Assert.That(result1, Is.Null);
        
        var result2 = Environment.GetEnvironmentVariable("UNKNOWN_ENV_VARIABLE") ?? "default value";
        Assert.That(result2, Is.EqualTo("default value"));
    }
}