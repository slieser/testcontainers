using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using NUnit.Framework;

namespace testcontainerexample.tests;

public class IntegrationTestsBase
{
    private IContainer? _testcontainers;

    private const int MongoDbPort = 27017;

    [SetUp]
    public async Task Setup() {
        Config.Load(".env");
        var containerBuilder = new ContainerBuilder()
            .WithImage("docker.io/mongo")
            .WithPortBinding(MongoDbPort, assignRandomHostPort: true)
            .WithEnvironment("MONGO_INITDB_ROOT_USERNAME", Config.MongoDb.User)
            .WithEnvironment("MONGO_INITDB_ROOT_PASSWORD", Config.MongoDb.Password)
            .WithWaitStrategy(Wait.ForUnixContainer().UntilPortIsAvailable(MongoDbPort));

        _testcontainers = containerBuilder.Build();
        await _testcontainers.StartAsync();
        Config.MongoDb.Port = _testcontainers.GetMappedPublicPort(MongoDbPort);
        Config.MongoDb.Host = _testcontainers.Hostname;
    }

    [TearDown]
    public async Task TeardownBase() {
        await _testcontainers!.StopAsync();
        await _testcontainers.DisposeAsync();
    }
}