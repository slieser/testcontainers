using NUnit.Framework;

namespace testcontainerexample.tests;

public class ConfigTests
{
    [SetUp]
    public void Setup() {
        Config.Load();
    }
    
    [Test]
    public void Values_are_read_from_env_file() {
        Config.Load(".env");
        Assert.That(Config.MongoDb.Host, Is.EqualTo("127.0.0.1"));
        Assert.That(Config.MongoDb.Port, Is.EqualTo(27017));
        Assert.That(Config.MongoDb.User, Is.EqualTo("mongo"));
        Assert.That(Config.MongoDb.Password, Is.EqualTo("secret"));
    }

    [Test]
    public void Some_values_can_be_changed() {
        Config.Load(".env");
        Assert.That(Config.MongoDb.Host, Is.EqualTo("127.0.0.1"));
        Config.MongoDb.Host = "changed";
        Assert.That(Config.MongoDb.Host, Is.EqualTo("changed"));
    }
}