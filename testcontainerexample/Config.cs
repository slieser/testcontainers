using dotenv.net;

namespace testcontainerexample;

public static class Config
{
    public static void Load() {
        Reset();
        DotEnv.Fluent().Load();
    }

    public static void Load(string filename) {
        Reset();
        DotEnv.Fluent().WithEnvFiles(filename).Load();
    }

    private static void Reset() {
        MongoDb._host = null;
        MongoDb._port = null;
    }

    public static class MongoDb
    {
        internal static string? _host;
        internal static int? _port;

        public static string Host {
            get => _host ?? GetEnvironmentVariable("MONGODB_HOST");
            set => _host = value;
        }
        
        public static int Port {
            get => _port ?? int.Parse(GetEnvironmentVariable("MONGODB_PORT"));
            set => _port = value;
        }

        public static string User => GetEnvironmentVariable("MONGODB_USER");
        
        public static string Password => GetEnvironmentVariable("MONGODB_PASSWORD");
    }
    
    private static string GetEnvironmentVariable(string variable) {
        var value = Environment.GetEnvironmentVariable(variable);
        if (value != null) {
            return value;
        }
        Load();
        value = Environment.GetEnvironmentVariable(variable);
        if (value != null) {
            return value;
        }
        return "";
    }
}