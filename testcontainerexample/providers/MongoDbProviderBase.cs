using MongoDB.Driver;

namespace testcontainerexample.providers;

public class MongoDbProviderBase
{
    protected readonly IMongoDatabase _database;

    public MongoDbProviderBase() {
        var credential = MongoCredential.CreateCredential(
            databaseName: "admin",   // mongodb database that contains the usernames
            username: Config.MongoDb.User,
            password: Config.MongoDb.Password
        );
        var settings = new MongoClientSettings {
            Server = new MongoServerAddress(Config.MongoDb.Host, Config.MongoDb.Port),
            Credential = credential
        };
        Console.WriteLine($"Mongo DB at {settings.Server.Host}:{settings.Server.Port}");
        var dbClient = new MongoClient(settings);
        _database = dbClient.GetDatabase("example");
    }
}